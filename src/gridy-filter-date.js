
import { TPL_PATH_AN } from "../../sk-core/src/sk-element.js";

import { FIELD_TITLE_AN, FILTER_MODE_AN } from "../../gridy-grid/src/filter/gridy-filter.js";

import { GridyFilterDriver } from "../../gridy-grid/src/filter/gridy-filter-driver.js";

import { DateParse } from "../../dateutils/src/global.js";

import {
	FILTER_MODE_LESS, FILTER_MODE_INCL_LESS, FILTER_MODE_GREATER,
	FILTER_MODE_INCL_GREATER, FILTER_MODE_CONTAINS, FILTER_MODE_EQUALS,
	FILTER_MODE_NOT_EQUALS
} from "../../gridy-grid/src/datasource/data-source-local.js";



export class GridyFilterDate extends GridyFilterDriver {

	get datePckerTn() {
		return this.comp.getAttribute('datepicker-tn') || 'sk-datepicker';
	}

	get tplPath() {
		if (!this._tplPath) {
			this._tplPath = this.comp.confValOrDefault(TPL_PATH_AN, '/node_modules/gridy-filter-date/src/tpls/') + 'gridy-filter-date.tpl.html';
		}
		return this._tplPath;
	}

	get inputEl() {
		if (!this._inputEl) {
			this._inputEl = this.comp.querySelector('#filterInput');
		}
		return this._inputEl;
	}

	get fmt() {
		return this.inputEl.fmt;
	}

	get mode() {
		return this.comp.hasAttribute(FILTER_MODE_AN) ? this.comp.getAttribute(FILTER_MODE_AN) : undefined;
	}

	compareDates(dataItemValue, value, mode, ctx) {
		dataItemValue = DateParse.parseDate(dataItemValue, ctx.fmt);
		value = DateParse.parseDate(value, ctx.fmt);
		if (value === "") {
			return true;
		}
		if (mode === FILTER_MODE_EQUALS) {
			if (dataItemValue.compareTo(value) === 0) {
				return true;
			}
		} else if (mode === FILTER_MODE_NOT_EQUALS) {
			if (dataItemValue.compareTo(value) !== 0) {
				return true;
			}
		} else if (mode === null || mode === undefined || mode === FILTER_MODE_CONTAINS) {
			if (dataItemValue && (dataItemValue).toString().indexOf(value) >= 0) {
				return true;
			}
		} else {
			if (mode === FILTER_MODE_GREATER) {
				if (dataItemValue.compareTo(value) > 0) {
					return true;
				}
			} else if (mode === FILTER_MODE_INCL_GREATER) {
				if (dataItemValue.compareTo(value) >= 0) {
					return true;
				}
			} else if (mode === FILTER_MODE_INCL_LESS) {
				if (dataItemValue.compareTo(value) <= 0) {
					return true;
				}
			} else if (mode === FILTER_MODE_LESS) {
				if (dataItemValue.compareTo(value) < 0) {
					return true;
				}
			}
		}
		return false;
	}

	renderFilter() {
		this.comp.tplVars = Object.assign({'fieldTitle': this.comp.fieldTitleLabel}, this.comp.tplVars);

		this.render().then(() => {
			if (this.inputHandler) {
				this.inputEl.removeEventListener('skchange', this.inputHandler);
			}
			let inputHandler = function (event) {
				let filters = [];
				if (this.inputEl.value) {
					filters.push(
						{
							value: this.inputEl.value, title: this.comp.getAttribute(FIELD_TITLE_AN),
							mode: this.mode, comparator: this.compareDates, ctx: this
						}
					);
				}
				this.comp.filters = filters;
				this.comp.changeFilter(event);
			}.bind(this);
			this.inputEl.addEventListener('skchange', inputHandler);

		});
	}
}
